# Prerequisites:
- Ubuntu server 18.04.3
- Docker installed on the server: https://docs.docker.com/engine/install/ubuntu/
- Docker compose installed on the server: `sudo apt-get install docker-compose`

# Quick Setup
1. Spin up a new ubuntu instance, and SSH into this new server.
2. Install docker following instructions in this link: https://docs.docker.com/engine/install/ubuntu/
3. Install docker-compose by running the following command: `sudo apt-get install docker-compose`
4. Clone the repo to the server: `git clone https://gitlab.com/caredove-inc-public/https-redirect.git`
5. `cd` into the cloned project, and run `nano init-letsencrypt.sh`. Edit line 8, replacing "HOST_DOMAIN" with the URL that will be redirected to another URL.
6. Run `./init-letsencrypt.sh`, which will generate your SSL certficate for the given host domain.
7. Run `cd data/nginx`, then `nano app.conf`, and replace "HOST_DOMAIN" with the redirect URL, and replace "TARGET_DOMAIN" with the domain you wish to redirect to.
8. Run `cd ..`, then `cd ..` again, then run `docker-compose up -d` to run the web server in the background. Use `docker logs nginx` to monitor the logs and ensure the server is running successfully.
9. Ensure your host domain's A record is pointed to the new server, and allow some time for the domain to update to target the new server. We should see a successful redirect once the domain is pointed correctly, and HTTPS://HOSTDOMAIN.CA should now redirect to HTTPS://TARGET_DOMAIN.CA.

# Useful resources:
Medium article, explaining the Docker setup, using Nginx and Certbot images: https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71
